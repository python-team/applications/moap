# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# routines to interface with Freshmeat through XML-RPC
# see http://freshmeat.net/faq/view/49/
# taken from "shipper" and adapted

import xmlrpclib
import netrc

# This is how we sanity-check against the XML-RPC API version.
required_major = "1"
required_minor = "02"

class SessionException(Exception):
    def __init__(self, message):
        self.message = message

    def __repr__(self):
        return '<SessionException: %s>' % self.message

    __str__ = __repr__

class SessionError(SessionException):
    """
    One of the XML-RPC errors from the Freshmeat API.
    """
    def __init__(self, code, message):
        self.code = code
        self.message = message

    def __repr__(self):
        return '<SessionError %d: %s>' % (self.code, self.message)

class Session:
    "Encapsulate the state of a Freshmeat session."
    freshmeat_xmlrpc = "http://freshmeat.net/xmlrpc/"

    def __init__(self, login=None, password=None, verbose=0):
        """
        Create an XML-RPC session to Freshmeat.
        """
        self.verbose = verbose
        self._sid = None # set to session ID if logged in

        # Open xml-rpc connection to Freshmeat
        self._session = xmlrpclib.Server(Session.freshmeat_xmlrpc,
                                         verbose=verbose)

    def _assert_logged_in(self):
        if not self._sid:
            raise SessionException("Not logged in")

    def fetch_available_licenses(self):
        return self._session.fetch_available_licenses()

    def login(self, login=None, password=None):
        # If user didn't supply credentials, fetch from ~/.netrc
        if not login:
            try:
                credentials = netrc.netrc()
            except netrc.NetrcParseError, e:
                raise SessionException("ill-formed .netrc: %s:%s %s" \
                                               % (e.filename, e.lineno, e.msg))
            except IOError, e:
                raise SessionException(("missing .netrc file %s" % \
                                                 str(e).split()[-1]))
            ret = credentials.authenticators("freshmeat")
            if not ret:
                raise SessionException("no credentials for Freshmeat")
            login, account, password = ret

        # Log in to Freshmeat
        response = self._session.login( \
            {"username":login, "password":password})
        self._sid = response['SID']
        self.lifetime = response['Lifetime']
        api_version = response['API Version']
        # Sanity-check against the version
        (major, minor) = api_version.split(".")
        if major != required_major or minor < required_minor:
            raise SessionException("this version is out of date; get a replacement from Freshmeat.")
        if self.verbose:
            print "Session ID = %s, lifetime = %s" % (self._sid, self.lifetime)

    def fetch_branch_list(self, project_name):
        "Get the branch list for the current project."
        self._assert_logged_in()
        if self.verbose:
            print "About to look up project"
        branch_list = self._session.fetch_branch_list({
            'SID': self._sid,
            'project_name': project_name
        })
        if self.verbose:
            print "Project branch list is:", branch_list
        return branch_list

    def fetch_release(self, project_name, branch_name, version):
        """
        @returns: dict with
                  release_focus, version, changes, hide_from_frontpage
        """
        self._assert_logged_in()
        args = ({
            'SID':          self._sid,
            'project_name': project_name,
            'branch_name':  branch_name,
            'version':      version,
        })

        try:
            result = self._session.fetch_release(args)
        except xmlrpclib.Fault, e:
            raise SessionError(e.faultCode, e.faultString)

        return result

    def publish_release(self, **data):
        """
        Add a new release.

        The "license" and "url_*" fields are optional and will be taken from
        the branch record if they are omitted from the submission. The
        'hide_from_frontpage' option can be omitted an defaults to 'do not
        hide'.

        @param project_name:        Project name to submit a release for
        @param branch_name:         Branch name to submit a release for
        @param version:             Version string of new release
        @param changes:             Changes list, no HTML,
                                    character limit 600 chars
                                    Empty will return error code 60.
        @param release_focus:       Release focus ID of new release
                                    (see Appendix A)
        @param hide_from_frontpage: 'Y' if release is to be hidden from 
                                    frontpage, everything else does not hide it
        @param license:             Branch license
        @param url_homepage:        Homepage
        @param url_tgz:             Tar/GZ
        @param url_bz2:             Tar/BZ2
        @param url_zip:             Zip
        @param url_changelog:       Changelog
        @param url_rpm:             RPM package
        @param url_deb:             Debian package
        @param url_osx:             OS X package
        @param url_bsdport:         BSD Ports URL
        @param url_purchase:        Purchase
        @param url_cvs:             CVS tree (cvsweb)
        @param url_list:            Mailing list archive
        @param url_mirror:          Mirror site
        @param url_demo:            Demo site
        """
        data['SID'] = self._sid
        try:
            response = self._session.publish_release(data)
        except xmlrpclib.Fault, e:
            raise SessionError(e.faultCode, e.faultString)

        if "OK" not in response:
            raise SessionException(response)

    def withdraw_release(self, release):
        response = self._session.withdraw_release(release)
        if "OK" not in response:
            raise SessionException(response)
  
    def logout(self):
        "End the session."
        ret = self._session.logout({
            "SID": self._sid
        })
        self._sid = None
        return ret

freshmeat_field_map = (
    ("Project",          "p", "project_name"),
    ("Branch",           "b", "branch_name"),
    ("Version",          "v", "version"),
    ("Changes",          "c", "changes"),
    ("Release-Focus",    "r", "release_focus"),
    ("Hide",             "x", "hide_from_frontpage"),
    ("License",          "l", "license"),
    ("Home-Page-URL",    "H", "url_homepage"),
    ("Gzipped-Tar-URL",  "G", "url_tgz"),
    ("Bzipped-Tar-URL",  "B", "url_bz2"),
    ("Zipped-Tar-URL",   "Z", "url_zip"),
    ("Changelog-URL",    "C", "url_changelog"),
    ("RPM-URL",	         "R", "url_rpm"),
    ("Debian-URL",	 "D", "url_deb"),
    ("OSX-URL",	         "O", "url_osx"),
    ("BSD-Port-URL",     "P", "url_bsdport"),
    ("Purchase-URL",     "U", "url_purchase"),
    ("CVS-URL",	         "S", "url_cvs"),
    ("Mailing-List-URL", "L", "url_list"),
    ("Mirror-Site-URL",  "M", "url_mirror"),
    ("Demo-URL",	 "E", "url_demo"),
    )
