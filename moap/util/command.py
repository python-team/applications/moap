# -*- Mode: Python; test-case-name: testsuite.test_util_command -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Command class.
"""
from moap.extern.log import log
from moap.extern.command import command

class LogCommand(command.Command, log.Loggable):
    def __init__(self, parentCommand=None, **kwargs):
        command.Command.__init__(self, parentCommand, **kwargs)
        self.logCategory = self.name

    # command.Command has a fake debug method, so choose the right one
    def debug(self, format, *args):
        kwargs = {}
        log.Loggable.doLog(self, log.DEBUG, -2, format, *args, **kwargs)

