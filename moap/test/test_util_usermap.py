# -*- Mode: Python; test-case-name: moap.test.test_doap_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import tempfile

from moap.util import usermap

from moap.test import common

class TestUserMap(common.TestCase):
    def setUp(self):
        self.map = usermap.UserMap()

    def testUserMapException(self):
        self.assertRaises(usermap.UserMapException, 
            self.map.parse, """thomaslisa
mama:papa""")

    def testUserMapFromFile(self):
        file = tempfile.NamedTemporaryFile(suffix='moap')
        file.write("""thomas:lisa
# this is a comment
mama:papa
""")
        file.flush() # otherwise nothing is written yet
        self.map.parseFromPath(file.name)
        self.assertEquals(self.map[0], ('thomas', 'lisa'))
        self.assertEquals(self.map[1], ('mama', 'papa'))
        self.assertEquals(len(self.map), 2)
        
