# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# code to send out a release announcement of the latest or specified version
# adapted from http://www.redcor.ch:8180/redcor/redcor/web/intranet_zope_plone/tutorial/faq/SendingMailWithAttachmentsViaPython

import smtplib
import MimeWriter
import base64
import StringIO

"""
Code to send out mails.
"""

class Message:
    """
    I create e-mail messages with possible attachments.
    """
    def __init__(self, subject, to, fromm):
        """
        @type  to:    string or list of strings
        @param to:    who to send mail to
        @type  fromm: string
        @param fromm: who to send mail as
        """
        self.subject = subject
        self.to = to
        if isinstance(to, str):
            self.to = [to, ]
        self.fromm = fromm
        self.attachments = [] # list of dicts

    def setContent(self, content):
        self.content = content

    def addAttachment(self, name, mime, content):
        d = {
            'name':    name,
            'mime':    mime,
            'content': content,
        }
        self.attachments.append(d)

    def get(self):
        """
        Get the message.
        """

        message = StringIO.StringIO()
        writer = MimeWriter.MimeWriter(message)
        writer.addheader('MIME-Version', '1.0')
        writer.addheader('Subject', self.subject)
        writer.addheader('To', ", ".join(self.to))

        writer.startmultipartbody('mixed')

        # start off with a text/plain part
        part = writer.nextpart()
        body = part.startbody('text/plain')
        body.write(self.content)

        # add attachments
        for a in self.attachments:
            part = writer.nextpart()
            part.addheader('Content-Transfer-Encoding', 'base64')
            body = part.startbody('%(mime)s; name=%(name)s' % a)
            body.write(base64.encodestring(a['content']))

        # finish off
        writer.lastpart()

        return message.getvalue()

    def send(self, server="localhost"):
        smtp = smtplib.SMTP(server)
        result = smtp.sendmail(self.fromm, self.to, self.get())
        smtp.close()

        return result
