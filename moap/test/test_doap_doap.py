# -*- Mode: Python; test-case-name: moap.test.test_doap_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.doap import doap

from moap.test import common

class TestDoap(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'doap', 'mach.doap')
        self.doap = doap.Doap()
        self.doap.addFile(file)

    def testGetProject(self):
        p = self.doap.getProject()
        self.failUnless(p)
        self.assertEquals(p.name, 'Mach')
        self.assertEquals(p.shortname, 'mach')
        self.failUnless(p.description, "No description")

        self.assertEquals(len(p.release), 11)
        v = p.release[0].version
        self.assertEquals(v.revision, '0.9.0')

        r = p.getRelease('0.9.0')
        self.failUnless(r)
        self.assertEquals(r.version.revision, '0.9.0')
        
        # 4 file releases in 0.9.0
        fr = r.version.file_release
        self.assertEquals(len(fr), 4)
        self.failUnless(fr[0].startswith('http'))

try:
    import RDF
except ImportError:
    TestDoap.skip = "No rdf module, skipping"
