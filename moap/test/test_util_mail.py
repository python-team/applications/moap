# -*- Mode: Python; test-case-name: moap.test.test_util_mail -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.util import mail

from moap.test import common

class TestMail(common.TestCase):
    def testMessage(self):
        m = mail.Message('subject', 'to', 'from')
        m.setContent('This is the message.')
        m.addAttachment('attachment', 'text/plain', 'This is the attachment.')
        output = m.get()

        self.failUnless(output.startswith(
            'Content-Type: multipart/mixed;'))
        self.failUnless('MIME-Version: 1.0' in output)
        self.failUnless('Subject: subject' in output)
        self.failUnless('To: to' in output)
        self.failUnless('This is the message.' in output)
        self.failUnless('Content-Type: text/plain;' in output)
        self.failUnless('Content-Transfer-Encoding:' in output)
        self.failUnless('name="attachment"' in output)
