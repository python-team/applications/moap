# -*- Mode: Python; test-case-name: moap.test.test_doap_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.util import ctags

from moap.test import common

class TestTag(common.TestCase):
    def setUp(self):
        self.tag = ctags.Tag()

    def testRepr(self):
        self.failUnless(repr(self.tag))

    def testParse(self):
        self.tag.parse('diff\tmoap/vcs/cvs.py\t/^    def diff(self, path):$/;"\tm\tline:98\tlanguage:Python\tclass:CVS')
        self.assertEquals(self.tag.name, "diff")
        self.assertEquals(self.tag.file, "moap/vcs/cvs.py")
        self.assertEquals(self.tag.line, 98)
        self.assertEquals(self.tag.language, 'Python')
        self.assertEquals(self.tag.klazz, 'CVS')

    def test281(self):
        # in moap.util.ctags, the tagMatcher originally had non-spaces for
        # the first column; ticket 281 has a space in the first column.
        self.tag.parse('line operator =\t/home/murrayc/svn/gnome220/branches/glom-1-6/glom/libglom/data_structure/field.cc\t/^Field& Field::operator=(const Field& src)$/;"\tf\tline:58\tlanguage:C++\tclass:Glom::Field\tsignature:(const Field& src)')

class TestCTags(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ctags', 'tags')
        self.ctags = ctags.CTags()
        self.ctags.addFile(file)

    def testGetManyTags(self):
        tags = self.ctags.getTags('moap/vcs/cvs.py', 93, 11)
        self.assertEquals(tags[0].name, 'commit')
        self.assertEquals(tags[1].name, 'diff')

    def testGetBeforeFirstTag(self):
        # asking for tags before there are any should return no tags
        tags = self.ctags.getTags('moap/vcs/cvs.py', 5, 6)
        self.failIf(tags)

    def testGetWithFirstTag(self):
        # asking for tags before and in first tag should give first tag
        tags = self.ctags.getTags('moap/vcs/cvs.py', 15, 6)
        self.assertEquals(len(tags), 1)
        self.assertEquals(tags[0].name, 'detect')

    def testGetTagBeforeTagLine(self):
        tags = self.ctags.getTags('moap/vcs/cvs.py', 15)
        self.failIf(tags)

    def testGetTagOnTagLine(self):
        tags = self.ctags.getTags('moap/vcs/cvs.py', 16)
        self.assertEquals(len(tags), 1)
        self.assertEquals(tags[0].name, 'detect')

    def testGetTagAfterTagLine(self):
        tags = self.ctags.getTags('moap/vcs/cvs.py', 17)
        self.assertEquals(len(tags), 1)
        self.assertEquals(tags[0].name, 'detect')

    def testGetLastTwo(self):
        # update starts on 106
        tags = self.ctags.getTags('moap/vcs/cvs.py', 105, 2)
        self.assertEquals(len(tags), 2)
        self.assertEquals(tags[0].name, 'diff')
        self.assertEquals(tags[1].name, 'update')

    def testGetLastTag(self):
        tags = self.ctags.getTags('moap/vcs/cvs.py', 106)
        self.assertEquals(len(tags), 1)
        self.assertEquals(tags[0].name, 'update')

class TestCTagsFromString(common.TestCase):
    def testFromEmptyString(self):
        self.ctags = ctags.CTags()
        self.ctags.addString('')

    def testFromString(self):
        self.ctags = ctags.CTags()
        self.ctags.addString('!_TAG_')

    def testFromWrongString(self):
        self.ctags = ctags.CTags()
        self.assertRaises(KeyError, self.ctags.addString, 'wrongtype')
