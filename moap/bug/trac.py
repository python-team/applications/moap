# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import xmlrpclib

from moap.bug import bug

def detect(URL):
    # FIXME: do smarter stuff in there
    if URL.find('trac') == -1:
        return None

    if URL.find('/newticket') > -1:
        URL = URL[:URL.find('/newticket')]

    return URL

class Trac(bug.BugTracker):
    def __init__(self, URL):
        bug.BugTracker.__init__(self, URL)
        self.debug('Contacting server at %s' % self.URL)
        self._server = xmlrpclib.ServerProxy(self.URL + '/xmlrpc')

    def getBug(self, id):
        """
        @type id: str
        """
        try:
            ticket = self._server.ticket.get(int(id))
        except xmlrpclib.Fault, e:
            # FIXME: a Fault has a faultCode and faultString
            # faultCode is 2 both for "not exist" and "id is not integer"
            # so we use a fragile string search, boo
            if e.faultString.find(' does not exist') > -1:
                raise bug.NotFoundError(id)

            # something else is wrong, reraise
            raise e

        if not ticket:
            return
        return self._getBugFromTicket(ticket)

    def query(self, queryString):
        # multicall.ticket is dynamically added
        __pychecker__ = 'no-objattrs'
        multicall = xmlrpclib.MultiCall(self._server)
        try:
            result = self._server.ticket.query(queryString)
        except xmlrpclib.Fault, e:
            raise e
        for r in result:
            multicall.ticket.get(r)

        ret = [self._getBugFromTicket(t) for t in multicall()]
        self.debug('Found %d bugs' % len(ret))
        return ret

    def _getBugFromTicket(self, ticket):
        """
        @type ticket: a list as returned by Trac's xml-rpc interface
        """
        bugId = ticket[0]
        d = ticket[3]
        summary = d['summary']
        return bug.Bug(bugId, summary)

BugClass = Trac        
