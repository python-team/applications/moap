# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
CVS functionality.
"""

import os
import re
import commands

import vcs

from moap.util import util

def detect(path):
    """
    Detect which version control system is being used in the source tree.

    @return: True if the given path looks like a CVS tree.
    """
    if not os.path.exists(os.path.join(path, 'CVS')):
        return False

    for n in ['Entries', 'Repository', 'Root']:
        if not os.path.exists(os.path.join(path, 'CVS', n)):
            return False

    return True

class CVS(vcs.VCS):
    name = 'CVS'

    def getUnknown(self, path):
        ret = []
        oldPath = os.getcwd()

        # FIXME: the only way I know of to get the list of unignored files
        # is to do cvs update, which of course has a side effect
        os.chdir(path)
        cmd = "cvs update"

        output = commands.getoutput(cmd)
        lines = output.split("\n")
        matcher = re.compile('^\?\s+(.*)')
        for l in lines:
            m = matcher.search(l)
            if m:
                path = m.expand("\\1")
                ret.append(path)

        # FIXME: would be nice to sort per directory
        os.chdir(oldPath)
        return ret

    def ignore(self, paths, commit=True):
        # cvs ignores entries by appending to a .cvsignore file in the parent
        oldPath = os.getcwd()
        os.chdir(self.path)

        tree = self.createTree(paths)
        toCommit = []
        for path in tree.keys():
            # this does the right thing if path == ''
            cvsignore = os.path.join(path, '.cvsignore')
            toCommit.append(cvsignore)

            new = False
            if not os.path.exists(cvsignore):
                new = True
                
            handle = open(cvsignore, "a")
                
            for child in tree[path]:
                handle.write('%s\n' % child)

            handle.close()

            if new:
                cmd = "cvs add %s" % cvsignore
                os.system(cmd)

        # FIXME: also commit .cvsignore flies when done.  Should we make
        # this part of the interface ?
        if commit and toCommit:
            cmd = "cvs commit -m 'moap ignore' %s" % " ".join(toCommit)
            os.system(cmd)

        os.chdir(oldPath)

    def commit(self, paths, message):
        oldPath = os.getcwd()
        os.chdir(self.path)
        temp = util.writeTemp([message, ])
        cmd = "cvs commit -F %s %s" % (temp, " ".join(paths))
        os.system(cmd)
        os.unlink(temp)
        os.chdir(oldPath)

    def diff(self, path):
        oldPath = os.getcwd()
        os.chdir(self.path)
        # CVS does not like running cvs diff with an absolute path
        if path.startswith(self.path):
            path = path[len(self.path) + 1:]
        # Don't want "Diffing ..." output, and 3 lines of context
        cmd = "cvs -q diff -u3 -N %s" % path
        self.debug('Running command %s' % cmd)
        output = commands.getoutput(cmd)
        os.chdir(oldPath)
        return output

    def update(self, path):
        cmd = "cvs update %s" % path
        status, output = commands.getstatusoutput(cmd)
        return output

VCSClass = CVS
