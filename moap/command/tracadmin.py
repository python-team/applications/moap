# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.util import util, usermap

# table name, column, filter query, multiple values possible
USER_TABLES = [
    ('permission',        'username',  None, False),
    ('auth_cookie',       'name',      None, False),
# it looks like the lines with authenticated == 0 are hashes, and with == 1
# are usernames
    ('session',           'sid',       'authenticated=1', False),
    ('session_attribute', 'sid',       'authenticated=1', False),
    ('wiki',              'author',    None, False),
    ('attachment',        'author',    None, False),
    ('ticket',            'owner',     None, False),
    ('ticket',            'reporter',  None, False),
    ('ticket_change',     'author',    None, False),
    ('ticket_change',     'oldvalue',
        "(field='qa_contact' OR field='owner')", True),
    ('ticket_change',     'newvalue', 
        "(field='qa_contact' OR field='owner' OR field='reporter')", True),
]

class List(util.LogCommand):
    summary = "list users in Trac database"

    def do(self, args):
        users = {}

        cxn = self.parentCommand.parentCommand.cxn
        c = cxn.cursor()

        for table, column, where, multiple in USER_TABLES:
            query = "SELECT %s FROM %s" % (column, table)
            if where:
                query += " WHERE %s" % where

            self.debug('Executing query %s' % query)
            c.execute(query)
            for row in c:
                if not row[0]:
                    continue

                # fields like qa_contact can have multiple users, separated
                # with ,
                if multiple:
                    names = row[0].split(',')
                    if len(names) > 1:
                        self.debug('Found multiple names: %s' % row[0])
                    for name in names:
                        users[name] = True
                else:
                    # verification
                    if row[0].find(',') > -1:
                        self.warning(
                            "table '%s', column '%s'"
                            " has multiple value '%s'." % (
                                table, column, row[0]))
                        continue
                    users[row[0]] = True

        # filter out "special" users
        for user in ['', 'anonymous', 'authenticated']:
            try:
                del users[user]
            except KeyError:
                pass

        userList = users.keys()
        userList.sort()
        for user in userList:
            self.stdout.write("%s\n" % user)

class Rename(util.LogCommand):
    summary = "rename a user in the Trac database"

    description = """Rename a user in the Trac database.

This updates all tables in the trac database where usernames are stored.
This operation obviously is non-reversible, so use with care.

Only tested with the sqlite backend of Trac, but since it uses the Trac
database API it should work with any backend.
"""

    def addOptions(self):
        self.parser.add_option('-u', '--usermap',
            action="store", dest="usermap",
            help="path to a file containing oldname:newname entries")

    def do(self, args):
        umap = usermap.UserMap()
        
        if self.options.usermap:
            umap.parseFromPath(self.options.usermap)
        else:
            try:
                old = args[0]
            except IndexError:
                self.stderr.write(
                    'Please specify the old username to change.\n')
                return 3

            try:
                new = args[1]
            except IndexError:
                self.stderr.write(
                    'Please specify the new username to change to.\n')
                return 3

            umap = usermap.UserMap()
            umap.parse("%s:%s" % (old, new))

        for old, new in umap:
            self.renameUser(old, new)

    def renameUser(self, old, new):
        self.debug('Renaming %s to %s' % (old, new))

        cxn = self.parentCommand.parentCommand.cxn

        for table, column, where, multiple in USER_TABLES:
            c = cxn.cursor()

            # first do all renames for non-multiple fields
            query = "UPDATE %(table)s SET %(column)s='%(new)s'" \
                " WHERE %(column)s='%(old)s'" % locals()
            if where:
                query += " AND %s" % where

            self.debug('Executing query %s' % query)
            c.execute(query)

            # now take tables into account that have multiple fields
            if multiple:
                c = cxn.cursor()
                query = "SELECT %(column)s FROM %(table)s" \
                    " WHERE %(column)s LIKE '%%,%%' " % locals()
                if where:
                    query += " AND %s" % where
                self.debug('Executing query %s' % query)
                c.execute(query)
                multiples = {}
                for row in c:
                    if not row[0]:
                        continue
                    names = row[0].split(',')
                    if not old in names:
                        continue
                    multiples[row[0]] = True

                for oldValue in multiples.keys():
                    # now that we know what to look for, we can update
                    self.stdout.write("Table '%s', column '%s' has value '%s'. "
                        "Please fix this manually.\n" % (
                            table, column, oldValue))
                    names = oldValue.split(',')
                    newNames = []
                    for name in names:
                        if name == old:
                            newNames.append(new)
                        else:
                            newNames.append(name)
                    newValue = ",".join(newNames)
                    
                    query = "UPDATE %(table)s SET %(column)s='%(newValue)s'" \
                " WHERE %(column)s='%(oldValue)s'" % locals()
                    if where:
                        query += " AND %s" % where

                    self.debug('Executing query %s' % query)
                    c.execute(query)


        # now commit all the changes
        cxn.commit()


class User(util.LogCommand):
    description = "Manage users."
    subCommandClasses = [List, Rename]

class TracAdmin(util.LogCommand):
    """
    @ivar path:        path to the Trac project environment
    @ivar cxn:         connection to Trac database
    @ivar environment: Trac environment
    @type environment: L{trac.env.Environment}
    """
    summary = "interact with a server-side trac installation"
    description = """Interact with a server-side trac installation.

This can be useful for maintainers of Trac installations.
"""
    subCommandClasses = [User]

    def addOptions(self):
        self.parser.add_option('-p', '--project',
            action="store", dest="project",
            help="path to Trac project")

    def handleOptions(self, options):
        self.path = options.project

        if not self.path:
            self.path = os.getcwd()

        # verify it is a trac project
        dbPath = os.path.join(self.path, 'db', 'trac.db')
        if not os.path.exists(dbPath):
            self.stderr.write("%s is not a Trac project.\n" % self.path)
            return 3

        from trac import env
        self.environment = env.Environment(self.path)
        self.cxn = self.environment.get_db_cnx()
