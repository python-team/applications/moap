%define pyver %(%{__python} -c "import sys; print sys.version[:3]")

Name:           moap
Version:        0.2.7
Release:        1%{?dist}
Summary:        Swiss army knife for project maintainers and developers
Source:         %{name}-%{version}.tar.bz2
URL:            http://thomas.apestaart.org/moap/trac/
License:        GPL
Group:          Applications/Archiving
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch

%description
MOAP is a swiss army knife for project maintainers and developers.
It aims to help in keeping you in the flow of maintaining, developing and
releasing, automating whatever tasks can be automated.

It allows you to parse DOAP files and submit releases, send release mails,
create iCal files and RSS feeds, maintain version control ignore lists,
check in based on the latest ChangeLog entry, and more.

%prep

%setup -q
%configure --sysconfdir=%{_sysconfdir}

%build

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc README moap.doap NEWS RELEASE ChangeLog
%{_bindir}/moap
%{_mandir}/man1/moap.1*
%{_libdir}/python*.*/site-packages/moap
%{_sysconfdir}/bash_completion.d/moap

%changelog
* Mon Aug 06 2007 Thomas Vander Stichele <thomas at apestaart dot org>
- added man page

* Sat Feb 03 2007 Thomas Vander Stichele <thomas at apestaart dot org>
- added bash completion file
- use DESTDIR install

* Sat Sep 16 2006 Thomas Vander Stichele <thomas at apestaart dot org>
- initial package
